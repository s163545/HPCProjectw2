target = poisson3d

.SUFFIXES:
.SUFFIXES: .f90 .o .c

CC = gcc
FC = gfortran
FFLAGS = -O3 -ffast-math -funroll-loops -fopenmp

OBJS = poisson3d.o precision.o m_processor.o m_poisdata.o init.o m_jacobi.o m_gauss_seidel.o write_vtk.o

LIBS =

.PHONY: all
all: $(target)

.PHONY: new
new: clean $(target)

.PHONY: clean realclean
clean:
	@/bin/rm -f $(OBJS) *.mod

realclean: clean
	@/bin/rm -f $(target)

# linking: the target depends on the objects
$(target): $(OBJS)
	$(FC) $(FFLAGS) $(OBJS) -o $(target)

.f90.o:
	$(FC) -c $(FFLAGS) $<

.c.o:
	$(CC) -c $<

# dependencies:
poisson3d.o: precision.o m_processor.o m_poisdata.o init.o m_jacobi.o m_gauss_seidel.o write_vtk.o
m_poisdata.o: precision.o
poisson_methods.o: precision.o
m_jacobi.o: precision.o m_poisdata.o m_processor.o
m_gauss_seidel.o: precision.o m_poisdata.o m_processor.o
m_processor.o: precision.o m_poisdata.o

