      MODULE m_gauss_seidel
      USE precision
      USE m_poisdata
      USE m_processor
   
      IMPLICIT NONE
       
      PRIVATE
      PUBLIC :: gauss_seidel

      CONTAINS
      
      SUBROUTINE gauss_seidel(u, f)
      
      REAL(wp), DIMENSION(:,:,:), INTENT(INOUT) :: u
      REAL(wp), DIMENSION(:,:,:), INTENT(IN) :: f
      INTEGER :: i, j, k, n
      REAL(wp) :: utemp, normsum

      PRINT*,"Running Gauss-Seidel solver"
     
      ! Outer loop 
      DO n=1,itermax
      
         DO k=2,Ni-1
            DO j=2,Ni-1
               DO i=2,Ni-1
                  utemp = u(i,j,k)
                  u(i,j,k) = 1.0_wp/6.0_wp*(  u(i-1,j,k) + u(i+1,j,k) &
                                            + u(i,j-1,k) + u(i,j+1,k) &
                                            + u(i,j,k-1) + u(i,j,k+1) &
                                                     + del**2*f(i,j,k) )     
                  normsum = normsum + ( u(i,j,k) - utemp )**2
               ENDDO
            ENDDO
         ENDDO
          
      ENDDO         
    
      ! Global error
      PRINT*," Global error:  = ",NORM2(u-f/( 3.0_wp*pi**2) )
      PRINT*," Relative global error  = ", NORM2(u-f/( 3.0_wp*pi**2) )/&
                                              NORM2(f/(3.0_wp*pi**2) )

 
      END SUBROUTINE 

      END MODULE 
