!---------------------------------------------------------------------------------------!
      SUBROUTINE initialize
      USE m_poisdata
      USE m_processor
      IMPLICIT NONE

      INTEGER :: i, j, k
      REAL(wp) :: mLi2, fact
            
      CALL read_namelist()
  
      PRINT*,"Ni =",Ni

      ALLOCATE(u(Ni,Ni,Ni), STAT=iostat)
      CALL check_iostat(iostat, "could not allocate 'u' matrix!")
      ALLOCATE(f(Ni,Ni,Ni), STAT=iostat)
      CALL check_iostat(iostat, "could not allocate 'f' matrix!")

      ! Initialize/define values
      del = Li/REAL(Ni-1,wp)
      mLi2 = -Li/2.0_wp
      fact = 3.0_wp*pi**2
      
      ! Apply initial conditions
      DO k=1,Ni
          z = mLi2 + REAL(k-1,wp)*del            
          DO j=1,Ni
             y = mLi2 + REAL(j-1,wp)*del            
             DO i=1,Ni
               x = mLi2 + REAL(i-1,wp)*del          
                 
               ! Dirichlet boundary conditions
               IF     ( i==1  ) THEN                        ! At x = -1
                  u(i,j,k) = 0.0_wp
               ELSEIF ( i==Ni ) THEN                        ! At x = +1
                  u(i,j,k) = 0.0_wp
               ELSEIF ( j==1  ) THEN                        ! At y = -1
                  u(i,j,k) = 0.0_wp
               ELSEIF ( j==Ni ) THEN                        ! At y = +1
                  u(i,j,k) = 0.0_wp
               ELSEIF ( k==1  ) THEN                        ! At z = -1
                  u(i,j,k) = 0.0_wp
               ELSEIF ( k==Ni ) THEN                        ! At z = +1
                  u(i,j,k) = 0.0_wp
               ELSE 
                  u(i,j,k) = 0.0_wp
               ENDIF
 
               ! Form right hand side (source term)
               f(i,j,k) = fact*SIN(pi*x)*SIN(pi*y)*SIN(pi*z)
            ENDDO
         ENDDO
      ENDDO          
      
 
!      PRINT*," u = "
!      DO k=1,Ni
!         DO i=1,Ni
!            PRINT"(I2.2,5(F8.4,TR1))",k,u(i,1:Ni,k)
!         ENDDO 
!      ENDDO
!
!      PRINT*," f = "
!      DO k=1,Ni
!         DO i=1,Ni
!            PRINT"(I2.2,5(F8.4,TR1))",k,f(i,1:Ni,k)
!         ENDDO 
!      ENDDO
!      

      END SUBROUTINE initialize
!---------------------------------------------------------------------------------------!
