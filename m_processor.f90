!----------------------------------------------------------------------------------------!

      MODULE m_processor
      USE precision     
      USE m_poisdata

      CONTAINS
!----------------------------------------------------------------------------------------!
      subroutine read_namelist()
          integer :: unit, io_err
          
          namelist /INPUT/ Ni, itermax, T0, tolerance, output, algorithm
            
          ! open and read file
          unit = 128273598
          open(unit, FILE="input.dat", action='read', iostat=io_err)
          call check_iostat(io_err, &
              "Could not open file 'input.dat', perhaps it does not exist?")
          read(unit, nml=INPUT, iostat=io_err)
          call check_iostat(io_err, &
              "Error on reading name-list, please correct file content")
          close(unit)

        end subroutine read_namelist
!----------------------------------------------------------------------------------------!
        subroutine check_iostat(iostat, msg)
          integer, intent(in) :: iostat
          character(len=*), intent(in) :: msg

          if ( iostat == 0 ) return

          write(*,'(a,i0,/,tr3,a)') 'ERROR = ', iostat, msg
          stop

        end subroutine check_iostat
!----------------------------------------------------------------------------------------!
        subroutine write_binary(u)
          ! Array to write-out
          real(wp), intent(in) :: u(:,:,:)

          ! Local variables
          character(len=*), parameter :: filename = 'poisson_u.bin'
          integer :: N
          integer :: iostat

          integer, parameter :: unit = 11249

          ! Get size of the array
          N = size(u, 1)

          ! replace == overwrite
          open(unit,file=filename,form='unformatted',access='stream',status='replace',&
                                                                           iostat=iostat)
          call check_iostat(iostat, "Could not open file '"//filename//"'!")
          write(unit, iostat=iostat) N
          call check_iostat(iostat, "Could not write variable N")
          write(unit, iostat=iostat) u
          call check_iostat(iostat, "Could not write variable u")
          close(unit)

        end subroutine write_binary
!----------------------------------------------------------------------------------------!
        subroutine write_vtk(u)

          ! This is C-interoperability using bindings
          use, intrinsic :: iso_c_binding

          interface
            subroutine write_vtk_c(n, u) BIND(C, name="write_vtk")
              import c_ptr, c_int
              implicit none
              integer(c_int), value :: n
              type(c_ptr), value :: u
            end subroutine
          end interface

          ! Array to write-out
          real(wp), intent(in), target :: u(:,:,:)

          ! Local variables
          integer(c_int) :: N

          ! Get size of the array
          N = size(u, 1)

          call write_vtk_c(n, c_loc(u(1,1,1)))

        end subroutine write_vtk
!----------------------------------------------------------------------------------------!

      END MODULE m_processor

