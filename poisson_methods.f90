module poisson_methods_m

  ! Please do not put any computational variable in this module.
  ! Pass all variables as arguments to the subroutine.
  use precision, only: dp ! use dp as the data-type (and for defining constants!)

  implicit none

  private
  public :: jacobi
  public :: gauss_seidel

contains

  subroutine jacobi( )

  end subroutine jacobi

  subroutine gauss_seidel( )

  end subroutine gauss_seidel

end module poisson_methods_m
