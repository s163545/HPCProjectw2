!---------------------------------------------------------------------------------------!
      MODULE m_poisdata
      USE precision
      IMPLICIT NONE

      ! This module contains the global variables/data used in the 3D poisson solver

      ! Define options for user
         ! No. of grid points
      INTEGER, SAVE :: Ni = 100                            
         ! Temperature at cold wall
      REAL(wp), SAVE :: T0 = 0._dp                         
         ! Maximum no. of iterations
      INTEGER, SAVE :: itermax = 1000                      
         ! Numerical tolerance
      REAL(wp), SAVE :: tolerance = 1.e-4_dp              
         ! Output setting:    0: No output, 3: Binary, 4: VTK
      INTEGER, SAVE :: output = 0
         ! Algorithm setting: 1: Jacobi, 2: Gauss-Seidel
      INTEGER, SAVE :: algorithm = 1
      
      ! Allocate global variables/data     
         ! Current temperature field array
      REAL(wp), ALLOCATABLE, DIMENSION(:,:,:) :: u
         ! Old/previous temperature field array
      REAL(wp), ALLOCATABLE, DIMENSION(:,:,:) :: u_old
         ! Right hand side/source term
      REAL(wp), ALLOCATABLE, DIMENSION(:,:,:) :: f
         ! Grid spacing in all directions (regular equidistant grid)
      REAL(wp) :: del
         ! Room size (domain size)
      REAL(wp), SAVE :: Li=2.0_wp
         ! Coordinates
      REAL(wp) :: x, y, z   
            
         ! Integer to determine I/O status
      INTEGER :: iostat
      
       

      END MODULE 
!---------------------------------------------------------------------------------------!
