module precision

  implicit none

  integer, parameter :: sp = KIND(1.0E0) !selected_real_kind(6,30)
  integer, parameter :: dp = KIND(1.0D0) ! selected_real_kind(14,100)
  integer, parameter :: wp = dp
  integer, parameter :: lp = selected_int_kind(18)

  REAL(wp), PARAMETER :: pi=ACOS(-1.0_wp)

end module precision
