!---------------------------------------------------------------------------------------!
      MODULE m_jacobi
      USE precision
      USE m_poisdata 
      USE m_processor

      IMPLICIT NONE
      
      PRIVATE
      PUBLIC :: jacobi      
 
      CONTAINS 

      SUBROUTINE jacobi(u, u_old, f)

      REAL(wp), DIMENSION(:,:,:), INTENT(INOUT) :: u
      REAL(wp), DIMENSION(:,:,:), ALLOCATABLE, INTENT(INOUT) :: u_old
      REAL(wp), DIMENSION(:,:,:), INTENT(IN) :: f
      INTEGER :: i, j, k, n 
      REAL(wp) :: utemp, normsum
      PRINT*,"Running jacobi solver"

      ALLOCATE( u_old(Ni,Ni,Ni), STAT=iostat )
      CALL check_iostat(iostat, "Could not allocate u_old matrix")

      ! Initialize u_old ( same as u )
      DO k=1,Ni
         DO j=1,Ni
            DO i=1,Ni
               u_old(i,j,k) = u(i,j,k) 
            ENDDO
         ENDDO
      ENDDO      
     
 
      ! Outer loop      
      DO n=1,itermax

         ! Loop through all nodes 
         normsum = 0.0_wp
         DO k=2,Ni-1
            DO j=2,Ni-1
               DO i=2,Ni-1
                  utemp = u(i,j,k)
                  u(i,j,k) = 1.0_wp/6.0_wp*(  u_old(i-1,j,k) + u_old(i+1,j,k) &
                                            + u_old(i,j-1,k) + u_old(i,j+1,k) &
                                            + u_old(i,j,k-1) + u_old(i,j,k+1) &
                                                           +  del**2*f(i,j,k) )
                  normsum = normsum + ( u(i,j,k)-utemp )**2
               ENDDO
            ENDDO
         ENDDO
               
         ! Swap u and u_old
         DO k=1,Ni
            DO j=1,Ni
               DO i=1,Ni
                  u_old(i,j,k) = u(i,j,k) 
               ENDDO
            ENDDO
         ENDDO      

      ENDDO       
     
      ! Global error
      PRINT*," Global error:  = ",NORM2(u-f/( 3.0_wp*pi**2) )
      PRINT*," Relative global error  = ", NORM2(u-f/( 3.0_wp*pi**2) )/&
                                              NORM2(f/(3.0_wp*pi**2) )
!
!     PRINT*," itermax = ",itermax      
 
      END SUBROUTINE jacobi

      END MODULE m_jacobi
!---------------------------------------------------------------------------------------!
