!---------------------------------------------------------------------------------------!
!---------------------------------------------------------------------------------------!
program poisson3d

  ! Import different methods
  use precision
  USE m_jacobi
  USE m_gauss_seidel
  USE m_poisdata  
  USE m_processor 

  !$OMP PARALLEL DEFAULT(NONE) SHARED(u,u_old,f,algorithm) 

  ! Initalize variables and set initial conditions
  CALL initialize
   
  IF    ( algorithm == 1 ) THEN
     CALL jacobi(u, u_old, f)
  ELSEIF( algorithm == 2) THEN
     CALL gauss_seidel(u, f)
  ENDIF

  !$OMP END PARALLEL

  deallocate(f)
  if ( allocated(u_old) ) deallocate(u_old)

  ! Keep u until we have written in out
  select case ( output )
  case ( 0 ) ! pass, valid but not used value
    ! do nothing
  case ( 3 ) ! write to binary file
    call write_binary(u)
  case ( 4 ) ! write to vtk file
    call write_vtk(u)
  case default

    write(*,'(a,i0)') 'Unknown output type requested: ', output
    stop

  end select
    
  deallocate(u)
end program poisson3d
!---------------------------------------------------------------------------------------!
!---------------------------------------------------------------------------------------!
